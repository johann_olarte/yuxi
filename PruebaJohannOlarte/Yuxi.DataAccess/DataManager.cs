﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Yuxi.Entities;

namespace Yuxi.DataAccess
{
    public class DataManager
    {
        static string storeProcedure = "";
        static DataTable data;
        static List<SqlParameter> parameters;

        public static bool IsXmlFile(string file)
        {
            return file != null &&
                file.EndsWith(".xml", StringComparison.Ordinal);
        }

        public static List<Book> GetListedBooks(XmlDocument xmlfileUploaded)
        {
            List<Book> BookList = new List<Book>();

            XmlNodeList books = xmlfileUploaded.GetElementsByTagName("Books");

            XmlNodeList book = ((XmlElement)books[0]).GetElementsByTagName("Book");

            foreach (XmlElement node in book)
            {
                Book objBook = new Book();
                objBook.Title = node["Title"].InnerText;
                objBook.Author = node["Author"].InnerText;
                objBook.Country = node["Country"].InnerText;
                objBook.Language = node["Language"].InnerText;
                objBook.Price = int.Parse(node["Price"].InnerText);
                objBook.Quantity = int.Parse(node["Quantity"].InnerText);

                BookList.Add(objBook);

                //SaveBooks(objBook);
            }

            return BookList;
        }

        public static List<Book> GetBooksSpanishLanguage(List<Book> allBooks)
        {
            List<Book> spanishBooksList = new List<Book>();

            allBooks.ForEach(delegate(Book book)
            {
                if (book.Language == "Spanish")
                {
                    spanishBooksList.Add(book);
                }
            });

            return spanishBooksList;
        }

        public static List<string> GetNameFrenchBooks(List<Book> allBooks)
        {
            List<string> nameFrenchBooksList = new List<string>();

            allBooks.ForEach(delegate(Book book)
            {
                if (book.Language == "French")
                {
                    nameFrenchBooksList.Add(book.Title);
                }
            });

            return nameFrenchBooksList;
        }

        public static List<string> GetNameBooksPriceOver(List<Book> allBooks)
        {
            List<string> nameBooksPriceOverList = new List<string>();

            allBooks.ForEach(delegate(Book book)
            {
                if(book.Price >= 100)
                {
                    nameBooksPriceOverList.Add(book.Title);
                }
            });

            return nameBooksPriceOverList;
        }

        public static List<Book> GetBooksPriceLow(List<Book> allBooks)
        {
            List<Book> booksPriceLowList = new List<Book>();

            allBooks.ForEach(delegate(Book book)
            {
                if (book.Price < 100)
                {
                    booksPriceLowList.Add(book);
                }
            });

            return booksPriceLowList;
        }

        public static bool RegisterQueryResults(string queryParameter, List<Book> books)
        {
            string queryResult = "";
            DateTime queryDate = DateTime.Today;
            storeProcedure = "spy_RegisterQueryResults";

            parameters = new List<SqlParameter>();
            SqlParameter QueryParameter = new SqlParameter("@queryParameter", SqlDbType.NVarChar);
            SqlParameter QueryResult = new SqlParameter("@queryResult", SqlDbType.NVarChar);
            SqlParameter QueryDate = new SqlParameter("@queryDate", SqlDbType.DateTime);

            books.ForEach(delegate(Book book)
            {
                queryResult += book.Title + " " + book.Author + " " + book.Country
                    + " " + book.Language + " " + book.Price.ToString() + " " + book.Quantity.ToString() + "; ";
            });


            QueryParameter.Value = queryParameter;
            QueryResult.Value = queryResult;
            QueryDate.Value = queryDate.ToString();

            parameters.Add(QueryParameter);
            parameters.Add(QueryResult);
            parameters.Add(QueryDate);

            Connection.SelectStoreProcedure(storeProcedure, parameters);

            return true;
        }

        public static bool RegisterQueryResults(string queryParameter, List<string> nameBooks)
        {
            string queryResult = "";
            DateTime queryDate = DateTime.Today;
            storeProcedure = "spy_RegisterQueryResults";

            parameters = new List<SqlParameter>();
            SqlParameter QueryParameter = new SqlParameter("@queryParameter", SqlDbType.NVarChar);
            SqlParameter QueryResult = new SqlParameter("@queryResult", SqlDbType.NVarChar);
            SqlParameter QueryDate = new SqlParameter("@queryDate", SqlDbType.DateTime);
            
            foreach(string bookName in nameBooks)
            {
                queryResult += bookName + "; ";
            }


            QueryParameter.Value = queryParameter;
            QueryResult.Value = queryResult;
            QueryDate.Value = queryDate.ToString();

            parameters.Add(QueryParameter);
            parameters.Add(QueryResult);
            parameters.Add(QueryDate);

            Connection.SelectStoreProcedure(storeProcedure, parameters);

            return true;
        }

        /// <summary>
        /// DB Connection and Transact test method
        /// </summary>
        /// <param name="books"></param>
        public static void SaveBooks(Book books)
        {
            storeProcedure = "spy_InsertBook";

            parameters = new List<SqlParameter>();
            SqlParameter title = new SqlParameter("@title", SqlDbType.NVarChar);
            SqlParameter author = new SqlParameter("@author", SqlDbType.NVarChar);
            SqlParameter country = new SqlParameter("@country", SqlDbType.NVarChar);
            SqlParameter language = new SqlParameter("@language", SqlDbType.NVarChar);
            SqlParameter price = new SqlParameter("@price", SqlDbType.Decimal);
            SqlParameter quantity = new SqlParameter("@quantity", SqlDbType.Int);

            title.Value =books.Title;
            author.Value = books.Author;
            country.Value = books.Country;
            language.Value = books.Language;
            price.Value = books.Price;
            quantity.Value = books.Quantity;

            parameters.Add(title);
            parameters.Add(author);
            parameters.Add(country);
            parameters.Add(language);
            parameters.Add(price);
            parameters.Add(quantity);

            Connection.SelectStoreProcedure(storeProcedure, parameters);
        }
    }
}
