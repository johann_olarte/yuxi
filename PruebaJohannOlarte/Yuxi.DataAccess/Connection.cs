﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;

namespace Yuxi.DataAccess
{
    public class Connection
    {
        private static SqlConnection connection =
            new SqlConnection(
                ConfigurationManager.ConnectionStrings["YuxiTestConnectionString"].ConnectionString
            );

        private static bool Connect()
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                return true;
            }
            catch (Exception)
            {
                throw new Exception("Error al conectar a la base de datos");
            }
        }

        private static void Disconnect()
        {
            connection.Close();
        }

        public static DataTable SelectStoreProcedure(string ProcName, List<SqlParameter> ParaArr)
        {
            DataTable data = new DataTable();

            if (Connect())
            {
                SqlCommand cmd = new SqlCommand(ProcName, connection);

                foreach (SqlParameter para in ParaArr)
                {
                    cmd.Parameters.Add(para);
                }

                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(data);
            }
            else
            {
                throw new Exception("No fue posible conectarse a la base de datos");
            }

            Disconnect();

            return data;
        }
    }
}
