﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using Yuxi.BusinessLogic;
using Yuxi.Entities;

namespace Yuxi.Presentation
{
    public partial class Default : System.Web.UI.Page
    {
        List<Book> bookList;
        List<string> selectedNameBooks;
        List<Book> selectedBooks;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack && this.bookList == null)
            {
                this.bookList = (List<Book>)Session["Books"];
            }
        }

        private void ShowErrors(string errorMessage)
        {
            this.lblMessages.Text = errorMessage;
        }

        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            try
            {
                XmlDocument _xmlFileUploaded = new XmlDocument();

                if (LogicManager.IsXmlFile(this.uploadXmlBooks.FileName))
                {

                    _xmlFileUploaded.Load(this.uploadXmlBooks.FileContent);

                    Session["Books"] = LogicManager.GetListedBooks(_xmlFileUploaded);

                    this.lblMessages.Text = "Archivo XML cargado, proceda a realizar las consultas";

                }
                else
                {
                    this.ShowErrors("El achivo no es el correcto");
                }
            }
            catch (Exception ex)
            {
                this.ShowErrors("Error: " + ex.Message);
            }
        }

        protected void btnGetAllBooks_Click(object sender, EventArgs e)
        {
            try
            {
                this.gvAllBooks.DataSource = this.bookList;
                this.gvAllBooks.DataBind();
                if (LogicManager.RegisterQueryResults("Get all books", this.bookList))
                {
                    this.lblMessages.Text = "Query OK";
                }
            }
            catch(Exception ex)
            {
                this.ShowErrors("Error: " + ex.Message);
            }
        }

        protected void btnGetBooksSpanishLanguage_Click(object sender, EventArgs e)
        {
            try
            {
                this.selectedBooks = LogicManager.GetBooksSpanishLanguage(this.bookList);
                this.gvSpanishBooks.DataSource = this.selectedBooks;
                this.gvSpanishBooks.DataBind();

                if (LogicManager.RegisterQueryResults("Get Spanish books", this.selectedBooks))
                {
                    this.lblMessages.Text = "Query OK";
                }
            }
            catch(Exception ex)
            {
                this.ShowErrors("Error: " + ex.Message);
            }
        }

        protected void btnGetNameFrenchBooks_Click(object sender, EventArgs e)
        {
            try
            {
                this.selectedNameBooks = LogicManager.GetNameFrenchBooks(this.bookList);
                this.gvFrenchBooks.DataSource = this.selectedNameBooks;
                this.gvFrenchBooks.DataBind();

                if (LogicManager.RegisterQueryResults("Get name French books", this.selectedNameBooks))
                {
                    this.lblMessages.Text = "Query OK";
                }
            }
            catch (Exception ex)
            {
                this.ShowErrors("Error: " + ex.Message);
            }
        }

        protected void btnGetNameBooksPriceOver_Click(object sender, EventArgs e)
        {
            try
            {
                this.selectedNameBooks = LogicManager.GetNameBooksPriceOver(this.bookList);
                this.gvPriceOverBooks.DataSource = this.selectedNameBooks;
                this.gvPriceOverBooks.DataBind();

                if (LogicManager.RegisterQueryResults("Get books name with price lower 100", this.selectedNameBooks))
                {
                    this.lblMessages.Text = "Query OK";
                }
            }
            catch (Exception ex)
            {
                this.ShowErrors("Error: " + ex.Message);
            }
        }

        protected void btnGetBooksPriceLow_Click(object sender, EventArgs e)
        {
            try
            {
                this.selectedBooks = LogicManager.GetBooksPriceLow(this.bookList);
                this.gvPriceLowBooks.DataSource = this.selectedBooks;
                this.gvPriceLowBooks.DataBind();

                if (LogicManager.RegisterQueryResults("Get books with price lower 100", this.selectedBooks))
                {
                    this.lblMessages.Text = "Query OK";
                }
            }
            catch (Exception ex)
            {
                this.ShowErrors("Error: " + ex.Message);
            }
        }
    }
}