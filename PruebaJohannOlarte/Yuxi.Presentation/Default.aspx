﻿<%@ Page Title="" Language="C#" 
    MasterPageFile="~/Master.Master" 
    AutoEventWireup="true" 
    CodeBehind="Default.aspx.cs" 
    Inherits="Yuxi.Presentation.Default" %>

<asp:Content ID="headDefault" ContentPlaceHolderID="headMaster" runat="server">
</asp:Content>
<asp:Content ID="contentDefault" ContentPlaceHolderID="bodyContentMaster" runat="server">
    <div>
        <p>Prueba Técnica Johann Olarte</p>
    </div>
    <div>
        <asp:FileUpload ID="uploadXmlBooks" runat="server" />
    </div>
    <div>
        <asp:Button runat="server" ID="btnUploadFile" OnClick="btnUploadFile_Click" Text="Cargar archivo"/>
        <asp:Label ID="lblMessages" runat="server"></asp:Label>
    </div>
    <div>
        <label>La informacion de todos los libros</label>
        <asp:Button runat="server" ID="btnGetAllBooks" Text="Mostrar" OnClick="btnGetAllBooks_Click" />
        <asp:GridView runat="server" ID="gvAllBooks"></asp:GridView>
    </div>
    <div>
        <label>La informacion de los libros en lenguaje español</label>
        <asp:Button runat="server" ID="btnGetBooksSpanishLanguage" Text="Mostrar" OnClick="btnGetBooksSpanishLanguage_Click" />
        <asp:GridView runat="server" ID="gvSpanishBooks"></asp:GridView>
    </div>
    <div>
        <label>El nombre de los libros en lenguaje frances</label>
        <asp:Button runat="server" ID="btnGetNameFrenchBooks" Text="Mostrar" OnClick="btnGetNameFrenchBooks_Click" />
        <asp:GridView runat="server" ID="gvFrenchBooks"></asp:GridView>
    </div>
    <div>
        <label>El nombre de los libros cuyo precio sea mayor o igual a 100</label>
        <asp:Button runat="server" ID="btnGetNameBooksPriceOver" Text="Mostrar" OnClick="btnGetNameBooksPriceOver_Click" />
        <asp:GridView runat="server" ID="gvPriceOverBooks"></asp:GridView>
    </div>
    <div>
        <label>La informacion de los libros cuya cantidad sea menor a 100</label>
        <asp:Button runat="server" ID="btnGetBooksPriceLow" Text="Mostrar" OnClick="btnGetBooksPriceLow_Click" />
        <asp:GridView runat="server" ID="gvPriceLowBooks"></asp:GridView>
    </div>
</asp:Content>
