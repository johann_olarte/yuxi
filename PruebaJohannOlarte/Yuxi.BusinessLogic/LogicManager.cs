﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Yuxi.DataAccess;
using Yuxi.Entities;

namespace Yuxi.BusinessLogic
{
    public class LogicManager
    {
        public static bool IsXmlFile(string file)
        {
            try
            {
                return DataManager.IsXmlFile(file);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public static List<Book> GetListedBooks(XmlDocument xmlfileUploaded)
        {
            try
            {
                return DataManager.GetListedBooks(xmlfileUploaded);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<Book> GetBooksSpanishLanguage(List<Book> allBooks)
        {
            try
            {
                return DataManager.GetBooksSpanishLanguage(allBooks);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<string> GetNameFrenchBooks(List<Book> allBooks)
        {
            try
            {
                return DataManager.GetNameFrenchBooks(allBooks);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<string> GetNameBooksPriceOver(List<Book> allBooks)
        {
            try
            {
                return DataManager.GetNameBooksPriceOver(allBooks);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public static List<Book> GetBooksPriceLow(List<Book> allBooks)
        {
            try
            {
                return DataManager.GetBooksPriceLow(allBooks);
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public static bool RegisterQueryResults(string queryParameter , List<Book> books)
        {
            try
            {
                return DataManager.RegisterQueryResults(queryParameter, books);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static bool RegisterQueryResults(string queryParameter, List<string> nameBooks)
        {
            try
            {
                return DataManager.RegisterQueryResults(queryParameter, nameBooks);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
