USE [master]
GO
/****** Object:  Database [YuxiPruebaJohannOlarte]    Script Date: 10/03/2015 11:25:00 ******/
CREATE DATABASE [YuxiPruebaJohannOlarte] ON  PRIMARY 
( NAME = N'YuxiPruebaJohannOlarte', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\YuxiPruebaJohannOlarte.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'YuxiPruebaJohannOlarte_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\YuxiPruebaJohannOlarte_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [YuxiPruebaJohannOlarte].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ANSI_NULLS OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ANSI_PADDING OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ARITHABORT OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET  DISABLE_BROKER
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET  READ_WRITE
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET RECOVERY SIMPLE
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET  MULTI_USER
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET DB_CHAINING OFF
GO
USE [YuxiPruebaJohannOlarte]
GO
/****** Object:  Table [dbo].[Parameter_Book]    Script Date: 10/03/2015 11:25:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Parameter_Book](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Parameter] [nvarchar](50) NOT NULL,
	[QueryResult] [nvarchar](1024) NOT NULL,
	[QueryDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Parameter_Book_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Parameter_Book] ON
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (1, N'Get all books', N'The Divine Comedy Dante Alighieri Italy Italian 100 1000; Pride and Prejudice Jane Austen United Kingdom English 70 250; Don Quixote Miguel de Cervant', CAST(0x00000000000007D2 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (2, N'Get all books', N'The Divine Comedy Dante Alighieri Italy Italian 100 1000; Pride and Prejudice Jane Austen United Kingdom English 70 250; Don Quixote Miguel de Cervantes Spain Spanish 300 95; Crime and Punishment Fyodor Dostoevsky Russia Russian 62 95; The Castle Franz Kafka Austria German 112 450; Love in the Time of Cholera Gabriel García Márquez Colombia Spanish 74 139; Sentimental Education Gustave Flaubert France French 265 12; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; Gypsy Ballads Federico', CAST(0x0000A52600000000 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (3, N'Get French books', N'The Divine Comedy Dante Alighieri Italy Italian 100 1000; Pride and Prejudice Jane Austen United Kingdom English 70 250; Don Quixote Miguel de Cervantes Spain Spanish 300 95; Crime and Punishment Fyodor Dostoevsky Russia Russian 62 95; The Castle Franz Kafka Austria German 112 450; Love in the Time of Cholera Gabriel García Márquez Colombia Spanish 74 139; Sentimental Education Gustave Flaubert France French 265 12; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; Gypsy Ballads Federico', CAST(0x0000A52600000000 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (4, N'Get French books', N'The Divine Comedy Dante Alighieri Italy Italian 100 1000; Pride and Prejudice Jane Austen United Kingdom English 70 250; Don Quixote Miguel de Cervantes Spain Spanish 300 95; Crime and Punishment Fyodor Dostoevsky Russia Russian 62 95; The Castle Franz Kafka Austria German 112 450; Love in the Time of Cholera Gabriel García Márquez Colombia Spanish 74 139; Sentimental Education Gustave Flaubert France French 265 12; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; Gypsy Ballads Federico', CAST(0x0000A52600000000 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (5, N'Get French books', N'The Divine Comedy Dante Alighieri Italy Italian 100 1000; Pride and Prejudice Jane Austen United Kingdom English 70 250; Don Quixote Miguel de Cervantes Spain Spanish 300 95; Crime and Punishment Fyodor Dostoevsky Russia Russian 62 95; The Castle Franz Kafka Austria German 112 450; Love in the Time of Cholera Gabriel García Márquez Colombia Spanish 74 139; Sentimental Education Gustave Flaubert France French 265 12; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; Gypsy Ballads Federico', CAST(0x0000A52600000000 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (6, N'Get French books', N'Sentimental Education; ', CAST(0x0000A52600000000 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (7, N'Get books name with price lower 100', N'The Divine Comedy; Don Quixote; The Castle; Sentimental Education; ', CAST(0x0000A52600000000 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (8, N'Get books with price lower 100', N'Pride and Prejudice Jane Austen United Kingdom English 70 250; Crime and Punishment Fyodor Dostoevsky Russia Russian 62 95; Love in the Time of Cholera Gabriel García Márquez Colombia Spanish 74 139; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; Ulysses James Joyce Irish English 65 735; ', CAST(0x0000A52600000000 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (9, N'Get all books', N'The Divine Comedy Dante Alighieri Italy Italian 100 1000; Pride and Prejudice Jane Austen United Kingdom English 70 250; Don Quixote Miguel de Cervantes Spain Spanish 300 95; Crime and Punishment Fyodor Dostoevsky Russia Russian 62 95; The Castle Franz Kafka Austria German 112 450; Love in the Time of Cholera Gabriel García Márquez Colombia Spanish 74 139; Sentimental Education Gustave Flaubert France French 265 12; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; Gypsy Ballads Federico', CAST(0x0000A52600000000 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (10, N'Get Spanish books', N'Don Quixote Miguel de Cervantes Spain Spanish 300 95; Love in the Time of Cholera Gabriel García Márquez Colombia Spanish 74 139; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; ', CAST(0x0000A52600000000 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (11, N'Get name French books', N'Sentimental Education; ', CAST(0x0000A52600000000 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (12, N'Get all books', N'The Divine Comedy Dante Alighieri Italy Italian 100 1000; Pride and Prejudice Jane Austen United Kingdom English 70 250; Don Quixote Miguel de Cervantes Spain Spanish 300 95; Crime and Punishment Fyodor Dostoevsky Russia Russian 62 95; The Castle Franz Kafka Austria German 112 450; Love in the Time of Cholera Gabriel García Márquez Colombia Spanish 74 139; Sentimental Education Gustave Flaubert France French 265 12; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; Gypsy Ballads Federico', CAST(0x0000A52600000000 AS DateTime))
INSERT [dbo].[Parameter_Book] ([Id], [Parameter], [QueryResult], [QueryDate]) VALUES (13, N'Get all books', N'The Divine Comedy Dante Alighieri Italy Italian 100 1000; Pride and Prejudice Jane Austen United Kingdom English 70 250; Don Quixote Miguel de Cervantes Spain Spanish 300 95; Crime and Punishment Fyodor Dostoevsky Russia Russian 62 95; The Castle Franz Kafka Austria German 112 450; Love in the Time of Cholera Gabriel García Márquez Colombia Spanish 74 139; Sentimental Education Gustave Flaubert France French 265 12; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; Gypsy Ballads Federico García Lorca Spain Spanish 65 735; Ulysses James Joyce Irish English 65 735; ', CAST(0x0000A52600000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[Parameter_Book] OFF
/****** Object:  Table [dbo].[Book]    Script Date: 10/03/2015 11:25:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Book](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Author] [varchar](50) NOT NULL,
	[Country] [varchar](50) NOT NULL,
	[BookLanguage] [varchar](50) NOT NULL,
	[Price] [decimal](18, 0) NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_Book] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Book] ON
INSERT [dbo].[Book] ([Id], [Title], [Author], [Country], [BookLanguage], [Price], [Quantity]) VALUES (1, N'The Divine Comedy', N'Dante Alighieri', N'Italy', N'Italian', CAST(100 AS Decimal(18, 0)), 1000)
INSERT [dbo].[Book] ([Id], [Title], [Author], [Country], [BookLanguage], [Price], [Quantity]) VALUES (2, N'Pride and Prejudice', N'Jane Austen', N'United Kingdom', N'English', CAST(70 AS Decimal(18, 0)), 250)
INSERT [dbo].[Book] ([Id], [Title], [Author], [Country], [BookLanguage], [Price], [Quantity]) VALUES (3, N'Don Quixote', N'Miguel de Cervantes', N'Spain', N'Spanish', CAST(300 AS Decimal(18, 0)), 95)
INSERT [dbo].[Book] ([Id], [Title], [Author], [Country], [BookLanguage], [Price], [Quantity]) VALUES (4, N'Crime and Punishment', N'Fyodor Dostoevsky', N'Russia', N'Russian', CAST(62 AS Decimal(18, 0)), 95)
INSERT [dbo].[Book] ([Id], [Title], [Author], [Country], [BookLanguage], [Price], [Quantity]) VALUES (5, N'The Castle', N'Franz Kafka', N'Austria', N'German', CAST(112 AS Decimal(18, 0)), 450)
INSERT [dbo].[Book] ([Id], [Title], [Author], [Country], [BookLanguage], [Price], [Quantity]) VALUES (6, N'Love in the Time of Cholera', N'Gabriel García Márquez', N'Colombia', N'Spanish', CAST(74 AS Decimal(18, 0)), 139)
INSERT [dbo].[Book] ([Id], [Title], [Author], [Country], [BookLanguage], [Price], [Quantity]) VALUES (7, N'Sentimental Education', N'Gustave Flaubert', N'France', N'French', CAST(265 AS Decimal(18, 0)), 12)
INSERT [dbo].[Book] ([Id], [Title], [Author], [Country], [BookLanguage], [Price], [Quantity]) VALUES (8, N'Gypsy Ballads', N'Federico García Lorca', N'Spain', N'Spanish', CAST(65 AS Decimal(18, 0)), 735)
INSERT [dbo].[Book] ([Id], [Title], [Author], [Country], [BookLanguage], [Price], [Quantity]) VALUES (9, N'Gypsy Ballads', N'Federico García Lorca', N'Spain', N'Spanish', CAST(65 AS Decimal(18, 0)), 735)
INSERT [dbo].[Book] ([Id], [Title], [Author], [Country], [BookLanguage], [Price], [Quantity]) VALUES (10, N'Ulysses', N'James Joyce', N'Irish', N'English', CAST(65 AS Decimal(18, 0)), 735)
SET IDENTITY_INSERT [dbo].[Book] OFF
/****** Object:  StoredProcedure [dbo].[spy_RegisterQueryResults]    Script Date: 10/03/2015 11:25:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Johann Iván Olarte Arboleda
-- Create date: October/3/2015
-- Description:	Register a query result
-- =============================================

CREATE PROCEDURE [dbo].[spy_RegisterQueryResults]
	-- Parameters
	@queryParameter NVARCHAR(50),
	@queryResult NVARCHAR(1024),
	@queryDate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO Parameter_Book (
		Parameter,
		QueryResult,
		QueryDate
	)
	VALUES (
		@queryParameter,
		@queryResult,
		@queryDate
	)
END
GO
/****** Object:  StoredProcedure [dbo].[spy_InsertBook]    Script Date: 10/03/2015 11:25:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Johann Iván Olarte Arboleda
-- Create date: October/3/2015
-- Description:	Register a new book
-- =============================================

CREATE PROCEDURE [dbo].[spy_InsertBook]
	-- Parameters
	@title NVARCHAR(50),
	@author NVARCHAR(50),
	@country NVARCHAR(50),
	@language NVARCHAR(50),
	@price DECIMAL(18,0),
	@quantity INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO Book (
		Title,
		Author,
		Country,
		BookLanguage,
		Price,
		Quantity
	)
	VALUES (
		@title,
		@author,
		@country,
		@language,
		@price,
		@quantity
	)
END
GO
