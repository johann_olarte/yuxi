USE [master]
GO
/****** Object:  Database [YuxiPruebaJohannOlarte]    Script Date: 10/03/2015 11:26:08 ******/
CREATE DATABASE [YuxiPruebaJohannOlarte] ON  PRIMARY 
( NAME = N'YuxiPruebaJohannOlarte', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\YuxiPruebaJohannOlarte.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'YuxiPruebaJohannOlarte_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\YuxiPruebaJohannOlarte_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [YuxiPruebaJohannOlarte].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ANSI_NULLS OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ANSI_PADDING OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ARITHABORT OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET  DISABLE_BROKER
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET  READ_WRITE
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET RECOVERY SIMPLE
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET  MULTI_USER
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [YuxiPruebaJohannOlarte] SET DB_CHAINING OFF
GO
USE [YuxiPruebaJohannOlarte]
GO
/****** Object:  Table [dbo].[Parameter_Book]    Script Date: 10/03/2015 11:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Parameter_Book](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Parameter] [nvarchar](50) NOT NULL,
	[QueryResult] [nvarchar](1024) NOT NULL,
	[QueryDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Parameter_Book_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Book]    Script Date: 10/03/2015 11:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Book](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Author] [varchar](50) NOT NULL,
	[Country] [varchar](50) NOT NULL,
	[BookLanguage] [varchar](50) NOT NULL,
	[Price] [decimal](18, 0) NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_Book] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[spy_RegisterQueryResults]    Script Date: 10/03/2015 11:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Johann Iván Olarte Arboleda
-- Create date: October/3/2015
-- Description:	Register a query result
-- =============================================

CREATE PROCEDURE [dbo].[spy_RegisterQueryResults]
	-- Parameters
	@queryParameter NVARCHAR(50),
	@queryResult NVARCHAR(1024),
	@queryDate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO Parameter_Book (
		Parameter,
		QueryResult,
		QueryDate
	)
	VALUES (
		@queryParameter,
		@queryResult,
		@queryDate
	)
END
GO
/****** Object:  StoredProcedure [dbo].[spy_InsertBook]    Script Date: 10/03/2015 11:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Johann Iván Olarte Arboleda
-- Create date: October/3/2015
-- Description:	Register a new book
-- =============================================

CREATE PROCEDURE [dbo].[spy_InsertBook]
	-- Parameters
	@title NVARCHAR(50),
	@author NVARCHAR(50),
	@country NVARCHAR(50),
	@language NVARCHAR(50),
	@price DECIMAL(18,0),
	@quantity INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO Book (
		Title,
		Author,
		Country,
		BookLanguage,
		Price,
		Quantity
	)
	VALUES (
		@title,
		@author,
		@country,
		@language,
		@price,
		@quantity
	)
END
GO
