USE YuxiPruebaJohannOlarte

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Johann Iv�n Olarte Arboleda
-- Create date: October/3/2015
-- Description:	Register a new book
-- =============================================

CREATE PROCEDURE spy_InsertBook
	-- Parameters
	@title NVARCHAR(50),
	@author NVARCHAR(50),
	@country NVARCHAR(50),
	@language NVARCHAR(50),
	@price DECIMAL(18,0),
	@quantity INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO Book (
		Title,
		Author,
		Country,
		BookLanguage,
		Price,
		Quantity
	)
	VALUES (
		@title,
		@author,
		@country,
		@language,
		@price,
		@quantity
	)
END
GO