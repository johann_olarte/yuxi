USE YuxiPruebaJohannOlarte

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Johann Iv�n Olarte Arboleda
-- Create date: October/3/2015
-- Description:	Register a query result
-- =============================================

CREATE PROCEDURE spy_RegisterQueryResults
	-- Parameters
	@queryParameter NVARCHAR(50),
	@queryResult NVARCHAR(1024),
	@queryDate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO Parameter_Book (
		Parameter,
		QueryResult,
		QueryDate
	)
	VALUES (
		@queryParameter,
		@queryResult,
		@queryDate
	)
END
GO